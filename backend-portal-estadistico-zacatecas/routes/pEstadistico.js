var express = require('express');
var app = express();


var Perfil = require('../models/PerfilEstadistico');


// ==========================================
// Obtener todos los usuarios
// =========================================
app.get('/', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);
// ==========================================
// Obtener todos los Perfiles
// =========================================
    Perfil.find({})
        .exec((err, perfilesas) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error cargando perfiles',
                    errors: err
                });
            }
            Perfil.count({}, (err, conteo) => {
                res.status(200).json({
                    ok: true,
                    perfilesas: perfilesas,
                    total: conteo
                });
            });
        });
});
module.exports = app;
