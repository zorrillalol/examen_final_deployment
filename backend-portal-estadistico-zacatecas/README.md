# Backend del sistema estadistico de zacatecas
Este es el código necesario para establecer el backend 
conectado a MongoDB usando Mongoose.

Para ejecutarlo es necesario reconstruir los módulos
de node usando el comando 

```
npm install
```

## Instalaciónes necesarias

- Angular

- MongoDB

- nodemon


- Express

```
npm install express
```

- Sistema de carga de archivos de express

```
npm install fast-csv
```

- Librerias para conversion del csv

```
npm install express-fast-csv
```
```
npm install json2csv
```

- Mongoose

```
npm install mongoose
```