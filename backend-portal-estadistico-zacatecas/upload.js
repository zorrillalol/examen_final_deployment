var csv = require('fast-csv');
var mongoose = require('mongoose');
var PerfilEstadistico = require('./models/PerfilEstadistico');

exports.post = function (req, res) {
	if (!req.files)
		return res.status(400).send('No se cargo ningun archivo');
	
	var pEstadisticoFile = req.files.file;

	var pEstadistico = [];
		
	csv
	 .fromString(pEstadisticoFile.data.toString(), {
		 headers: true,
		 ignoreEmpty: true
	 },{encoding: 'utf-8'})
	 .on("data", function(data){
		 data['_id'] = new mongoose.Types.ObjectId();
		 
		 pEstadistico.push(data);
	 })
	 .on("end", function(){
		PerfilEstadistico.create(pEstadistico, function(err, documents) {
			if (err) {
				throw err;
			}else{
				res.send(pEstadistico.length + ' El archivo se ha subido correctamente');
			}
		 });
	 });
};