// Requires
var express = require('express');
var fileUpload = require('express-fileupload');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');


// Inicializar variables


var app = express();

// CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    next();
});
var appPerfiles = require('./routes/pEstadistico');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(fileUpload());
// Conexión a la BD
mongoose.connection.openUri('mongodb://localhost:27017/portalEstadisticoDB', (err, res) => {
    if (err) throw err;

});
//Envia todos los perfiles a la pagina
app.use('/perfiles', appPerfiles);

//CARGA CSV
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
  });
  
  var template = require('./template.js');
  app.get('/template', template.get);
  
  var upload = require('./upload.js');
  app.post('/', upload.post);

// Escuchar peticiones
app.listen(4201, () => {
});



