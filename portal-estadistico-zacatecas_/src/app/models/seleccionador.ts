export class Seleccionador {

    constructor (
        public Sexo?: string,
        public Estado?: string,
        public Municipio?: string,
        public Poblacion?: string,
        public EstadoCivil?:string,
        public Servicios?:string,
        public Escolaridad?:number,
        public Estudia?:string,
        public DerechoH?:string,
        public Adiccion?:string,
        public Trabaja?:string,
        public Higiene?:string,
        public Planificacion?:string,
        public SaludB?:string,
        public Remedios?:string,
        public Cfamiliares?:string,
        public ECronica?:string,
        public Cmuerte?:string,
    ) { }

}
