export class GraficasModel {

    constructor (
        public labels: string[],
        public data: number[],
        public type:string,
        public leyenda:string
    ) { }

}
