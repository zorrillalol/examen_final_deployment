import { Injectable } from '@angular/core';

@Injectable()
export class SidebarService {

  menu: any = [
    {
      titulo: 'Principal',
      icono: 'mdi mdi-gauge',
      submenu: [
        { titulo: 'Dashboard', url: '/dashboard' },
        { titulo: 'Seleccionador', url: '/seleccionador' },
        { titulo: 'Acerca De', url: '/nosotros' },

        
      ]
    }
  ];

  constructor() { }

}
