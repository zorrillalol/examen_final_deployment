import { Injectable } from '@angular/core';
import { Seleccionador } from 'src/app/models/seleccionador';
import { Router } from '@angular/router';
import { GraficasModel } from 'src/app/models/graficas';

import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../../config/config';
import { map } from 'rxjs/operators';
import { Perfiles } from 'src/app/models/perfilesEstadisticos';

@Injectable({
  providedIn: 'root'
})
export class GraficasService {

  constructor(
    public http: HttpClient,
    public router: Router) {
  }

  totalP: number = 0 ;
  cantidad=0;
  poblaciones=0;
  viviendas=0;
  graficos: GraficasModel[]=[];
  perfilesT:Perfiles[]=[];
  edadesRangos:Number[]=[];
  datosSexos:number[]=[];
  datosEstadoCivil:number[]=[];
  leyenda:string = ''
  
  obtenerGraficas(seleccionador: Seleccionador) {
    this.graficos = [];
    this.generarGrafico(seleccionador)

    this.router.navigate(['/graficas1',JSON.stringify(this.graficos),this.leyenda])
    this.leyenda = ""
  }

   cargarGraficas() {

    let url = URL_SERVICIOS + '/perfiles';
    return this.http.get( url )
                .pipe( map( (resp: any) => {
                  this.totalP= resp.total;
                  
                  this.perfilesT = resp.perfilesas;
                  this.cargarEdades();
                  this.getCantidad();
                  return resp.perfilesas;

                }));

  }

  private filtrarPeticion(seleccionador: Seleccionador){
    var filtrado
    var filtro = {}
    var nombre = ""

    for (var chara in seleccionador){
      nombre = chara
      if (typeof seleccionador[chara] ==='string'){
        if (seleccionador[chara].length > 0){
          filtro[nombre]=seleccionador[chara]
          this.leyenda += nombre + ": " + seleccionador[chara] + ", "
        }
      }else if(typeof seleccionador[chara] ==='number'){
        if (seleccionador[chara] >= 0){
          filtro[nombre]=seleccionador[chara]
          this.leyenda += nombre + ": " + seleccionador[chara] + ", "
        }
      }
    }

    filtrado = this.perfilesT.filter(function(item) {
      for (var key in filtro) {
        if (item[key] === undefined || item[key] != filtro[key])
          return false;
        }
      return true;
    });

    return filtrado
  }

  public generarGrafico(seleccionador: Seleccionador){
      var arreglo = this.filtrarPeticion(seleccionador)

      var hombre= 0;
      var mujer = 0;
      
      var rango1=0;
      var rango2=0;
      var rango3=0;
      var rango4=0;
      var totlIndice = 0;

      for(let i in arreglo){
        totlIndice +=1;
        if(arreglo[i].Sexo == 'M'){
          hombre+=1;
        }else {
          mujer+=1;
        }
      }

    for(let i in arreglo){

      if (arreglo[i].Edad <10  && arreglo[i].Edad >= 0 ) {
        rango1+=1;
      }else if (arreglo[i].Edad >9 && arreglo[i].Edad <20) {
        rango2+=1;
      }else if (arreglo[i].Edad >19 && arreglo[i].Edad <60) {
        rango3+=1;
      }else {
        rango4+=1;
      }
    }

    this.graficos.push( new GraficasModel(['Hombre','Mujer'],[hombre,mujer],'pie','Busqueda Personalizada'))
    this.graficos.push( new GraficasModel(['Niños','Adolescentes','Adultos Jóvenes','Adultos Mayores'],
                                [rango1,rango2,rango3,rango4],'polarArea','Rango Edades en busqueda'))
  }

  private agruparSexo(){
    var hombre= 0;
    var mujer = 0;
    var totlIndice = 0;
    for(let i in this.perfilesT){
      totlIndice +=1;
      if(this.perfilesT[i].Sexo == 'F'){
        mujer+=1;
      }else {
        hombre+=1;
      }

    }

    this.datosSexos[0]=mujer;
    this.datosSexos[1]=hombre;
  }

  public getSexo(){
    this.agruparSexo();
    return this.datosSexos;
  }

  
  public getCantidad(){
    return this.cantidad;
  }

  private countViviendas(){
   var numMax = -Infinity
      for(let i in this.perfilesT){
        if (this.perfilesT[i].Vivienda > numMax){
          numMax = this.perfilesT[i].Vivienda
        }
      }
      if (numMax > 0){
        this.viviendas = numMax;
      }else this.viviendas = 0;
  }

  public getViviendas(){
    this.countViviendas()
    return this.viviendas
  }

  private countPoblaciones(){
    var poblac = new Set();
    var p = ""
    for (let i in this.perfilesT) {
      p = this.perfilesT[i].Poblacion
      poblac.add(p)
    }
    this.poblaciones = poblac.size
  }

  public getPoblaciones(){
    this.countPoblaciones()
    return this.poblaciones
  }

  public cargarEdades(){
    let rango1=0;
    let rango2=0;
    let rango3=0;
    let rango4=0;
    this.cantidad =0;
    for(let i in this.perfilesT){

      if (this.perfilesT[i].Edad <10  && this.perfilesT[i].Edad >= 0 ) {
        rango1+=1;
      }else if (this.perfilesT[i].Edad >9 && this.perfilesT[i].Edad <20) {
        rango2+=1;
      }else if (this.perfilesT[i].Edad >19 && this.perfilesT[i].Edad <60) {
        rango3+=1;
      }else {
        rango4+=1;
      }
      this.cantidad +=1;
    }

    this.edadesRangos[0]=rango1;
    this.edadesRangos[1]=rango2;
    this.edadesRangos[2]=rango3;
    this.edadesRangos[3]=rango4;
    this.getCantidad();

    return this.edadesRangos;
  }



  public agruparEstadoCivil(){
    var casado= 0;
    var soltero = 0;
    var divorciado= 0;
    var viudo = 0;
    var desc = 0;
    var unionL = 0;
    var totlIndice = 0;
    for(let i in this.perfilesT){
      totlIndice +=1;
      if(this.perfilesT[i].EstadoCivil == 'Casado'){
        casado+=1;
      }else if(this.perfilesT[i].EstadoCivil == 'Soltero'){
        soltero+=1;
      }else if(this.perfilesT[i].EstadoCivil == 'Divorciado'){
        divorciado+=1;
      }else if(this.perfilesT[i].EstadoCivil == 'Viudo'){
        viudo+=1;
      }else if(this.perfilesT[i].EstadoCivil == 'Union Libre'){
        unionL+=1;
      }else if(this.perfilesT[i].EstadoCivil == 'Desconocido'){
        desc+=1;
      }
    }

      this.datosEstadoCivil[0]=casado;
      this.datosEstadoCivil[1]=soltero;
      this.datosEstadoCivil[2]=divorciado;
      this.datosEstadoCivil[3]=viudo;
      this.datosEstadoCivil[4]=unionL;
      this.datosEstadoCivil[5]=desc;
  }

  public getEstadoCivil(){
    this.agruparEstadoCivil();
    return this.datosEstadoCivil;
  }

  /**
  * obtener Enfermedades Cronicas
  */
  public obtenerECronicas() {
    var eCron = new Set<String>();
    var p = ""
    for (let i in this.perfilesT) {
      p = this.perfilesT[i].ECronica
      eCron.add(p)
    }
    return Array.from(eCron)
  }
}