import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { SeleccionadorInteractivoComponent } from './seleccionador-interactivo/seleccionador-interactivo.component';
import { Graficas1Component } from './graficas1/graficas1.component';
import { AccoutSettingsComponent } from './accout-settings/accout-settings.component';
import { AboutComponent } from './about/about.component';



const pagesRoutes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'seleccionador', component: SeleccionadorInteractivoComponent },
            { path: 'graficas1', component: Graficas1Component },
            { path: 'graficas1/:termino/:leyenda', component: Graficas1Component },
            { path: 'nosotros', component: AboutComponent },
            { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
        ]
    }
];


export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );
