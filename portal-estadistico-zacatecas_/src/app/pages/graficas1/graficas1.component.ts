import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Perfiles } from '../../models/perfilesEstadisticos';
import {  GraficasService} from '../../services/service.index';


import { GraficasModel } from 'src/app/models/graficas';

@Component({
  selector: 'app-graficas1',
  templateUrl: './graficas1.component.html',
  styles: []
})
export class Graficas1Component implements OnInit {
  grafico: GraficasModel[]=[];
  cantidad = 0;
  leyenda = '';
  perfiles: Perfiles[] = [];
  constructor(   public _graficasService:GraficasService , public activatedRoute: ActivatedRoute ) {
    activatedRoute.params
        .subscribe( params => {
          let termino = params.termino;
          this.grafico = JSON.parse(termino)
          this.leyenda = params.leyenda
          this.cantidad=this.grafico.length
        });
  }

  ngOnInit() {
    this.cargarHospitales();
    console.log(this.cantidad)
  }
  
  cargarHospitales() {
    this._graficasService.cargarGraficas()
        .subscribe( perfiles => this.perfiles = perfiles );
    this.cantidad = this._graficasService.totalP;

  }
}
