import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import {  GraficasService} from '../../services/service.index';
import { Perfiles } from '../../models/perfilesEstadisticos';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {

  

 

  pieChartLegend = true;
  public pieChartType: ChartType = 'pie';
  edadesRangos:Number[]=[];
  perfiles: Perfiles[] = [];
  sexoCantidad:number[]=[]
  cantidad:Number=0;
  poblaciones:number=0;
  viviendas:Number=0;
  datosEstadoCivil:number[]=[];
  doughnutChartLabels:string[]=[];
  doughnutChartData:number[]=[];
  
  
  constructor(public _graficasService:GraficasService) {

    this.cargarGraficas();
    this.cargarEstadoCivil();
    

  }
  

  ngOnInit() {

  }
  cargarGraficas() {

    this._graficasService.cargarGraficas()
        .subscribe( perfiles => {
          this.perfiles = perfiles;
          this.sexoCantidad = this._graficasService.getSexo();
          this.cantidad = this._graficasService.cantidad;
          this.datosEstadoCivil=this._graficasService.getEstadoCivil();
          this.edadesRangos = this._graficasService.cargarEdades();
          this.poblaciones = this._graficasService.getPoblaciones();
          this.viviendas = this._graficasService.getViviendas()
          this.doughnutChartData=[this.datosEstadoCivil[0],this.datosEstadoCivil[1],
          this.datosEstadoCivil[2],this.datosEstadoCivil[3],this.datosEstadoCivil[4],this.datosEstadoCivil[5]];
          } );
          

  }
  
  cargarEstadoCivil(){
    this.doughnutChartLabels = ['Casado', 'Soltero', 'Divorciado','Viudo','Union Libre','Desconocido'];
    
  }



}
