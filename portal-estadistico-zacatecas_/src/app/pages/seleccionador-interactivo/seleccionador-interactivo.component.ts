import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import {  Seleccionador } from '../../models/seleccionador';
import { GraficasService } from 'src/app/services/graficas/graficas.service';
import { Router } from '@angular/router';
import { equal } from 'assert';
@Component({
  selector: 'app-seleccionador-interactivo',
  templateUrl: './seleccionador-interactivo.component.html',
  styles: []
})
export class SeleccionadorInteractivoComponent {
  forma: FormGroup;
  estadoCB: any[] = ['Vivo','Muerto'];
  municipioCB: any[] = ['Guadalupe'];
  PoblacionCB: any[] = ['Tacoaleche','El Bordo','San Jose'];
  estadoCivilCB: any[] = ['Soltero','Casado','Divorciado','Viudo', 'Union Libre', 'Desconocido'];
  sexoCB: any[] = ['Hombre','Mujer'];
  nivelEstCB: any[] = ['Ninguna','Primaria','Secundaria','Bachillerato','Carrera Tecnica','Licenciatura'];
  servicioMedicoCB: any[] = ['Centro de salud','ISSSTE','Seguro Popular','Ninguno'];
  eleccionBI: any[] = ['Si','No'];
  eCronicas: any[] = [];

  seleccionador: Seleccionador;
  graficos: any ;
  constructor(public _graficos: GraficasService,public router: Router) {
    

  } 
  ngOnInit() {

    this.forma = new FormGroup({
    }, );
    this.seleccionador = new Seleccionador('','','','','','',-1,'','','','','','','','','','','')
    this.eCronicas = this._graficos.obtenerECronicas()
  }

  escogerPoblacion(PoblacionVM: string){
    
    this.seleccionador.Poblacion=PoblacionVM
    
  }

  escogerEstado(estadoMV: string){
    
    this.seleccionador.Estado=estadoMV
    
  }

  escogerMunicipio(MunicipioMV: string){
    
    this.seleccionador.Municipio=MunicipioMV
  }

  escogerEstadoCivil(estadoc: string){
    this.seleccionador.EstadoCivil=estadoc
  }

  escogerSexo(s: string){
    if(s === "Mujer"){
      this.seleccionador.Sexo='F'
    }else{
      this.seleccionador.Sexo='M'
    }
  }

  escogerServicioMedico(sm: string){
    this.seleccionador.Servicios=sm
  }

  escogerNivelEst(NEstudio: string){
    this.seleccionador.Escolaridad=this.nivelEstCB.indexOf(NEstudio)
  }

  escogerEnfermedad(eCronica:string){
    this.seleccionador.ECronica=eCronica
  }

  escogerEstudia(estudia: string){
    this.seleccionador.Estudia = estudia
  }

  escogerDerechoH(derechoH: string){
    this.seleccionador.DerechoH = derechoH
  }

  escogerAdicto(addicion: string){
    this.seleccionador.Adiccion = addicion
  }

  escogerTrabaja(trabaja: string){
    this.seleccionador.Trabaja = trabaja
  }

  escogerHigiene(higiene: string){
    this.seleccionador.Higiene = higiene
  }

  escogerPlan(planifica: string){
    this.seleccionador.Planificacion = planifica
  }

  escogerSalud(saludB: string){
    this.seleccionador.SaludB = saludB
  }

  escogerReme(remedio: string){
    this.seleccionador.Remedios = remedio
  }

  escogerCFamiliares(cFamiliares){
    this.seleccionador.Cfamiliares = cFamiliares
  }

  obtenerEstadisticas( ) {
    this._graficos.obtenerGraficas(this.seleccionador)         
  }
  }